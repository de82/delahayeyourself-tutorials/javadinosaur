package javadinosaur;

import javadinosaur.dinosaurs.Dinosaur;
import javadinosaur.dinosaurs.Orientation;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sam
 */
public class DinosaurTest {

    /**
     * This is a dummy method for testing sayHello method from Dinosaur
     * Class, it should return a String like "Hello ! I'm <Dinosaur.name> !"
     * This is only for educational purpose and a damn stupid example.
     */
    @Test
    public void testSayHello(){
        Dinosaur nessy = new Dinosaur();
        Dinosaur freddy = new Dinosaur("Freddy");

        assertEquals("Hello ! I'm Nessy !", nessy.sayHello());
        assertEquals("Hello ! I'm Freddy !", freddy.sayHello());
    }
    
    @Test
    public void testTurn(){
        Dinosaur nessy = new Dinosaur();
        
        assertEquals(Orientation.North, nessy.getOrientation());
        nessy.turn();
        assertEquals(Orientation.East, nessy.getOrientation());
        nessy.turn();
        assertEquals(Orientation.South, nessy.getOrientation());
        nessy.turn();
        assertEquals(Orientation.West, nessy.getOrientation());
        nessy.turn();
        assertEquals(Orientation.North, nessy.getOrientation());
    }
}
