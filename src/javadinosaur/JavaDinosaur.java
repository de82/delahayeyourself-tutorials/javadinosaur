package javadinosaur;

import javadinosaur.dinosaurs.Dinosaur;

/**
 *
 * @author sam
 */
public class JavaDinosaur {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Dinosaur nessy = new Dinosaur();
        Dinosaur freddy = new Dinosaur("Freddy");
        
        System.out.println(String.format("Call to Nessy toString method: %s", nessy));
        System.out.println(String.format("Call to Nessy sayHello method: %s", nessy.sayHello()));
        System.out.println(String.format("Call to Freddy sayHello method: %s", freddy.sayHello()));
    }
    
}
