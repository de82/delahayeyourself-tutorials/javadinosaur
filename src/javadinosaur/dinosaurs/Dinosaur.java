/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadinosaur.dinosaurs;

/**
 *
 * @author sam
 */
public class Dinosaur {

    private String name;
    private Orientation orientation = Orientation.North;

    public Dinosaur() {
        this.name = "Nessy";
    }

    public Dinosaur(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public String sayHello() {
        return String.format("Hello ! I'm %s !", this.name);
    }

    public void turn() {
        switch (this.getOrientation()) {
            case North:
                this.setOrientation(Orientation.East);
                break;
            case East:
                this.setOrientation(Orientation.South);
                break;
            case South:
                this.setOrientation(Orientation.West);
                break;
            case West:
                this.setOrientation(Orientation.North);
                break;
        }
    }
}
