package javadinosaur.dinosaurs;

/**
 *
 * @author sam
 */
public enum Orientation {
    North,
    East,
    South,
    West;
}
